package com.saigoncomputer;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.saigoncomputer.helpers.Constant;
import com.saigoncomputer.helpers.FileUploadUtil;
import com.saigoncomputer.models.Product;
import com.saigoncomputer.services.ProductService;

import org.springframework.web.multipart.MultipartFile;



@Controller
public class AppController {
	
	@Autowired
	private ProductService productService;
	
	@RequestMapping("/")
	public String viewHomePage(Model model) {
		//return viewHomePage(model, 1);
		return viewHomePage(model, 1, "name", "asc");
	}
	
	@RequestMapping("/page/{pageNum}")
	public String viewHomePage(Model model, @PathVariable(name = "pageNum") int pageNum, @Param("sortName") String sortName,
	        @Param("sortDir") String direction) {
		
		if (sortName == null ) {
			sortName = "name";
		}
		
		if (direction == null) {
			direction = "asc";
		}
		
		Page<Product> page = productService.getProductPage(pageNum -1, sortName, direction);
	     
	    List<Product> listProduct = page.getContent();
	     
	    model.addAttribute("currentPage", pageNum);
	    model.addAttribute("totalPages", page.getTotalPages());
	    model.addAttribute("totalItems", page.getTotalElements());
	    
	    model.addAttribute("sortName", sortName);
	    model.addAttribute("direction", direction);
	    model.addAttribute("invertDirection", direction.equals("asc") ? "desc" : "asc");
	    
		model.addAttribute("listProduct", listProduct);
		
		return "index";
	}
	
	
	@RequestMapping("/new_product")
	public String showNewProduct(Model model) {
		Product product = new Product();
		
		model.addAttribute("product", product);
		
		return "new_product";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveProduct(@RequestParam("fileImage") MultipartFile multipartFile,
			@ModelAttribute("product") Product product) {
		
		String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		
		product.setImagePath(fileName);
		
		productService.save(product);
		
		String uploadDir = Constant.UPLOAD_FOLDER + "/" + product.getId();
		
		try {
			FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "redirect:/";
	}
	
	@RequestMapping("/edit_product/{id}")
	public ModelAndView showEditProduct(@PathVariable(name = "id") Long id) {
		ModelAndView modelAndView = new ModelAndView("edit_product");
		
		Product product = productService.get(id);
		
		modelAndView.addObject("product", product);
		
		//return "edit_product";
		return modelAndView;
	}
	
	@RequestMapping("/delete/{id}")
	public String deleteProduct(@PathVariable(name = "id") Long id) {
		productService.delete(id);
		return "redirect:/";
	}
}
