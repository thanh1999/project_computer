package com.saigoncomputer.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.saigoncomputer.dao.ProductRepository;
import com.saigoncomputer.helpers.Constant;
import com.saigoncomputer.models.Product;




@Service
public class ProductService {

	private static final int PAGE_SIZE = 0;
	@Autowired

	private ProductRepository repo;

	public Page<Product>getProductPage(int pageNum,String sortName,String direction){
		Pageable pageable = PageRequest.of(pageNum, Constant.PAGE_SIZE,direction.equals("asc")?Sort.by(sortName).ascending(): Sort.by(sortName).descending());
		return repo.findAll(pageable);
	}
	public List<Product> getAllProduct() {
		return repo.findAll();
	}
	
	public void save(Product product) {
		repo.save(product);
	}
	
	public Product get(Long id) {
		return repo.findById(id).get();
	}
	
	public void delete(Long id) {
		repo.deleteById(id);
	}
	public Page<Product>getProductPage(int pageNum) {
		Pageable pageable = PageRequest.of(pageNum,Constant.PAGE_SIZE);
		return repo.findAll(pageable);
	}
}

	

	


