package com.saigoncomputer.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.sql.SQLException;

import com.saigoncomputer.models.User;
public interface UserRepository extends CrudRepository<User,Integer>{
	@Query("SELECT u FROM User u WHERE u.username = :username")
	public User getUserByUserName(@Param("username") String username);
}
