package com.saigoncomputer.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.saigoncomputer.models.Product;



public interface ProductRepository extends JpaRepository<Product,Long>{
	
	public static final Integer PAGE_SIZE = 20;
}
