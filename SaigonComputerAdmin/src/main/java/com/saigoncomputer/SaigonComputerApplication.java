package com.saigoncomputer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class SaigonComputerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SaigonComputerApplication.class, args);
	}

}
