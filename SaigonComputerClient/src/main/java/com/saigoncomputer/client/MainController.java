package com.saigoncomputer.client;

import java.util.List;

import javax.validation.Valid;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.saigoncomputer.client.helpers.AuthProvider;
import com.saigoncomputer.client.helpers.ShopCartUtil;
import com.saigoncomputer.client.models.CartInfo;
import com.saigoncomputer.client.models.Customer;
import com.saigoncomputer.client.models.Product;
import com.saigoncomputer.client.models.ShipInfo;
import com.saigoncomputer.client.services.CustomerServices;
import com.saigoncomputer.client.services.OrderService;
import com.saigoncomputer.client.services.ProductService;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

@Controller
public class MainController {
	@Autowired
	private CustomerServices customerServices;

	@Autowired
	private ProductService productService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private JavaMailSender mailSender;

	@RequestMapping("/index")
	public String viewHomePage(Model model, @RequestParam(value = "type", defaultValue = "") String type) {

		List<Product> listLaptop = (List<Product>) productService.get("laptop");
		List<Product> listPc = (List<Product>) productService.get("pc");
		List<Product> listPhukien = (List<Product>) productService.get("phukien");
		List<Product> listMacbook = (List<Product>) productService.get("macbook");
		model.addAttribute("listLaptop", listLaptop);
		model.addAttribute("listPc", listPc);
		model.addAttribute("listPhukien", listPhukien);
		model.addAttribute("listMacbook", listMacbook);

		System.out.println(listLaptop.size());
		for (Product obj : listLaptop) {
			System.out.println(listLaptop.toString());
		}
		for (Product obj : listPc) {
			System.out.println(listPc.toString());
		}
		for (Product obj : listPhukien) {
			System.out.println(listPhukien.toString());
		}
		for (Product obj : listMacbook) {
			System.out.println(listMacbook.toString());
		}
		return "index";
	}

	@RequestMapping("/")
	public String viewHomePage2() {
		System.out.println("------------------------");
		return "redirect:/index";
	}

	@RequestMapping("/buyProduct")
	public String buyProductHandler(HttpServletRequest request, Model model,
			@RequestParam(value = "code", defaultValue = "") Long code) {

		if (code == null) {
			return "index";
		}

		Product product = productService.get(code);
		System.out.println(product.toString());
		if (product != null) {
			CartInfo cartInfo = ShopCartUtil.getCartInSession(request);
			cartInfo.addProduct(product, 1);
		}

		return "redirect:/shoppingCart";
	}

	@RequestMapping(value = { "/pc" }, method = RequestMethod.GET)
	public String viewPc(HttpServletRequest request, Model model) {
		List<Product> listProduct = productService.getAllProduct();

		model.addAttribute("listProduct", listProduct);

		return "pc";
	}

	@RequestMapping(value = { "/macbook" }, method = RequestMethod.GET)
	public String viewMacbook(HttpServletRequest request, Model model) {
		List<Product> listProduct = productService.getAllProduct();

		model.addAttribute("listProduct", listProduct);

		return "macbook";
	}

	@RequestMapping(value = { "/phukien" }, method = RequestMethod.GET)
	public String viewPhukien(HttpServletRequest request, Model model) {
		List<Product> product = productService.getAllProduct();

		return "phukien";
	}

	@RequestMapping(value = { "/laptop" }, method = RequestMethod.GET)
	public String viewLaptop(HttpServletRequest request, Model model) {
		List<Product> product = productService.getAllProduct();

		return "laptop";
	}

	@RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.GET)
	public String viewShoppingCart(HttpServletRequest request, Model model) {
		CartInfo myCart = ShopCartUtil.getCartInSession(request);

		model.addAttribute("cartForm", myCart);
		return "shoppingCart";
	}

	@RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.POST)
	public String updateShoppingCart(HttpServletRequest request, Model model,
			@ModelAttribute("cartForm") CartInfo cartForm) {
		CartInfo myCart = ShopCartUtil.getCartInSession(request);
		myCart.updateQuantity(cartForm);

		// model.addAttribute("cartForm" , myCart);
		return "redirect:/shoppingCart";
	}

	@RequestMapping(value = { "/shoppingCartShipInfo" }, method = RequestMethod.GET)
	public String shoppingCartShipInfo(HttpServletRequest request, Model model) {
		CartInfo cartInfo = ShopCartUtil.getCartInSession(request);
		if (cartInfo.isEmpty()) {
			return "redirect:/shoppingCart";
		}
		ShipInfo shipInfo = new ShipInfo();

		model.addAttribute("shipInfo", shipInfo);

		return "shoppingCartShipInfo";
	}

	@RequestMapping(value = { "/shoppingCartShipInfo" }, method = RequestMethod.POST)
	public String saveShipInfo(HttpServletRequest request, Model model, @ModelAttribute("shipInfo") ShipInfo shipInfo) {

		CartInfo cartInfo = ShopCartUtil.getCartInSession(request);
		cartInfo.setShipInfo(shipInfo);

		return "redirect:/shoppingCartConfirmation";
	}

	@RequestMapping(value = { "/shoppingCartConfirmation" }, method = RequestMethod.GET)
	public String viewShoppingCartConfirm(HttpServletRequest request, Model model) {

		CartInfo cartInfo = ShopCartUtil.getCartInSession(request);
		if (cartInfo.isEmpty()) {
			return "redirect:/shoppingCart";
		}
		model.addAttribute("myCart", cartInfo);

		return "shoppingCartConfirmation";
	}

	@RequestMapping(value = { "/shoppingCartConfirmation" }, method = RequestMethod.POST)
	public String saveShoppingCartConfirm(HttpServletRequest request, Model model) {
		CartInfo cartInfo = ShopCartUtil.getCartInSession(request);
		if (cartInfo.isEmpty()) {
			return "redirect:/shoppingCart";
		}

		// save order to database; -> generate order_num;
		orderService.saveOrder(cartInfo);

		// ->> gui email thong tin don hang + order_num

		ShopCartUtil.removeCartInSession(request);

		ShopCartUtil.saveLastCartInSession(request, cartInfo);

		return "redirect:/shoppingCartFinalize";
	}

	@RequestMapping("/shoppingCartFinalize")
	public String shoppingCartFinalize(HttpServletRequest request, Model model, String toMail) {
		CartInfo lastOrderedCart = ShopCartUtil.loadLastCartInSession(request);

		if (lastOrderedCart == null) {
			return "redirect:/shoppingCart";
		}
		model.addAttribute("lastOrderedCart", lastOrderedCart);

		return "shoppingCartFinalize";

	}

	@RequestMapping("/shoppingCartRemoveProduct")
	public String removeProductHandler(HttpServletRequest request, Model model,
			@RequestParam(value = "code", defaultValue = "") Long code) {

		Product product = productService.get(code);
		if (product != null) {
			CartInfo cartInfo = ShopCartUtil.getCartInSession(request);
			cartInfo.removeProduct(product);
		}

		return "redirect:/shoppingCart";
	}

	@GetMapping("/register")
	public String showRegister(Model model) {
		Customer customer = new Customer();

		model.addAttribute("customer", customer);

		return "signup";
	}

	@PostMapping("/register")
	public String postRegister(@Valid @ModelAttribute("customer") Customer customer, BindingResult bindingResult) {
		System.out.println("postRegister Email: " + customer.getEmail());
		System.out.println("postRegister Password: " + customer.getPassword());

		if (bindingResult.hasErrors()) {
			return "signup";
		}

		Customer currentCustomer = customerServices.getByEmail(customer.getEmail());

		if (currentCustomer == null) {
			customerServices.registerNewCustomer(customer, AuthProvider.BASIC);
			sendRegisterSuccessEmail(customer.getEmail());
			return "redirect:/index";
		}

		return "signup_failed";
	}

	@RequestMapping("/login")
	public String postLogin(@ModelAttribute("customer") Customer customer) {
		System.out.println("postLogin Email: " + customer.getEmail());
		System.out.println("postLogin Password: " + customer.getPassword());

		Customer currentCustomer = customerServices.getByEmail(customer.getEmail());

		if (currentCustomer == null) {
			customerServices.registerNewCustomer(customer, AuthProvider.BASIC);
			return "login";
		} else {
			if (customer.getPassword().equals(currentCustomer.getPassword())) {
				System.out.println("succeed");
				return "redirect:/index";
			} else {
				System.out.println("failed");
				return "login";
			}
		}
	}

	private void sendRegisterSuccessEmail(String toMail) {
		String fromMail = "duongminhthanh1999td@gmail.com";

		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);

		try {
			helper.setSubject("Saigoncomputer register successfully");
			helper.setFrom(fromMail);
			helper.setTo(toMail);

			helper.setText("<h1>Saigoncomputer</h1>, <br> <p>Ban da dang ky thanh cong</p>", true);

			mailSender.send(message);

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	@RequestMapping("/searchProduct")
	public String searchProduct(Model model, @RequestParam(value = "name", defaultValue = "") String name) {
		List<Product> listLaptop = (List<Product>) productService.search(name);
		List<Product> listPC = (List<Product>) productService.search(name);
		List<Product> listMacbook = (List<Product>) productService.search(name);
		List<Product> listPhukien = (List<Product>) productService.search(name);

		for (Product obj : listLaptop) {
			if (obj.getName().equals(name)) {
				System.out.println(obj.toString());
			}
		}

		for (Product obj : listPC) {
			if (obj.getName().equals(name)) {
				System.out.println(obj.toString());
			}
		}

		for (Product obj : listMacbook) {
			if (obj.getName().equals(name)) {
				System.out.println(obj.toString());
			}
		}
		

		for (Product obj : listPhukien) {
			if (obj.getName().equals(name)) {
				System.out.println(obj.toString());
			}
		}
		return "index";

	}
}
