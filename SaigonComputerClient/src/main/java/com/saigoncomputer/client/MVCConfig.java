package com.saigoncomputer.client;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.saigoncomputer.client.helpers.Constant;

@Configuration
public class MVCConfig implements WebMvcConfigurer {
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		System.out.println("MVCConfig::addViewControllers");
		registry.addViewController("/login").setViewName("login");

		// registry.addViewController("/register").setViewName("signup");
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		exposeDirectory(Constant.UPLOAD_FOLDER, registry);
		WebMvcConfigurer.super.addResourceHandlers(registry);
	}

	private void exposeDirectory(String dirName, ResourceHandlerRegistry registry) {
		Path uploadDir = Paths.get(dirName);

		String uploadPath = uploadDir.toFile().getAbsolutePath();

		System.out.println("--------------exposeDirectory: " + uploadPath);

		registry.addResourceHandler("/" + dirName + "/**").addResourceLocations("file:" + uploadPath + "/");
	}

}
