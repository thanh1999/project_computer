package com.saigoncomputer.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.saigoncomputer.client.handler.OAuthenticationSuccess;
import com.saigoncomputer.client.oauth2.CustomerOauth2UserService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfigure extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private CustomerOauth2UserService userService;
	
	@Autowired
	private OAuthenticationSuccess oauthenticationSuccess;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/","/#_=_","/index","/listProduct","/searchProduct","/shoppingCartShipInfo","/shoppingCartFinalize","/buyProduct","/shoppingCart","/shoppingCartConfirmation", "/register", "/login","/oauth2/**", "/assets/**", "/login_assets/**").permitAll()
			.anyRequest().permitAll()
			.and()
			.oauth2Login().loginPage("/login").permitAll()
			.userInfoEndpoint().userService(userService)
			.and().successHandler(oauthenticationSuccess)
			.and()
			.logout().permitAll()
			;
	}
}
