package com.saigoncomputer.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication

public class SaigonComputerClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SaigonComputerClientApplication.class, args);
	}

}
