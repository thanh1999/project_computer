package com.saigoncomputer.client.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.saigoncomputer.client.models.Customer;
public interface CustomerRepository extends JpaRepository<Customer, Integer>{
	
	@Query("SELECT customer FROM Customer customer WHERE customer.email = :email")
	public Customer getByEmail(@Param("email") String email);

	
}
