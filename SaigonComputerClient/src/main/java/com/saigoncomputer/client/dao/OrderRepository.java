package com.saigoncomputer.client.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.saigoncomputer.client.models.Order;
public interface OrderRepository extends CrudRepository<Order, Long>{
	@Query("SELECT order FROM Order order WHERE order.status = 'NEW'")
	public List<Order> getNewOrders();
}
