package com.saigoncomputer.client.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.saigoncomputer.client.models.Product;
import org.springframework.data.repository.query.Param;

public interface ProductRepository extends JpaRepository<Product, Long> {
	@Query("SELECT prd FROM Product prd where prd.type = :type")
	public List<Product> getProductByType(@Param("type") String type);

	@Query("SELECT prd FROM Product prd where prd.type = 'laptop'")
	public List<Product> getProductLaptop();

	@Query("SELECT prd FROM Product prd where prd.type = 'dienthoai'")
	public List<Product> getProductDienthoai();

	@Query("SELECT prd FROM Product prd where prd.type = 'pc'")
	public List<Product> getProductPc();

	@Query("SELECT prd FROM Product prd WHERE prd.name LIKE %?1%")
	public List<Product> search(String keyword);
}
