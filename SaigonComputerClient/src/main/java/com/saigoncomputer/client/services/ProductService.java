package com.saigoncomputer.client.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saigoncomputer.client.dao.ProductRepository;
import com.saigoncomputer.client.models.Product;

@Service
public class ProductService {
	@Autowired
private ProductRepository repo;
	
	public List<Product> getAllProduct() {
		return repo.findAll();
	}
	
	public List<Product> search(String name){
		return repo.search(name);
	}
	
	public void save(Product product) {
		repo.save(product);
	}
	
	public Product get(Long id) {
		return repo.findById(id).get();
	}
	
	public List<Product> get(String type) {
		return repo.getProductByType(type);
	}
	public void delete(Long id) {
		repo.deleteById(id);
	}
}
