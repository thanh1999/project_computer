package com.saigoncomputer.client.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saigoncomputer.client.dao.CustomerRepository;
import com.saigoncomputer.client.helpers.AuthProvider;
import com.saigoncomputer.client.models.Customer;


@Service
public class CustomerServices {
	@Autowired
	private CustomerRepository repo;
	
	public List<Customer> listAllCustomer() {
		return repo.findAll();
	}
	
	public Customer getByEmail(String email) {
		return repo.getByEmail(email);
	}

	public void registerNewCusomer(String email, String fullName, AuthProvider provider) {
		Customer customer = new Customer();
		Date date = new Date();
		
		customer.setEmail(email);
		customer.setFirstName(fullName);
		customer.setEnabled(true);
		customer.setCreatedTime(date);
		customer.setLastLogin(date);
		customer.setAuthProvider(provider);
		
		repo.save(customer);
	}

	public void updateCustomer(Customer customer, String fullName, AuthProvider provider) {
		
		customer.setFirstName(fullName);
		customer.setAuthProvider(provider);
		customer.setLastLogin(new Date());
		
		repo.save(customer);
			
	}

	public void registerNewCustomer(Customer customer, AuthProvider provider) {
		Date date = new Date();
		
		customer.setEnabled(true);
		customer.setCreatedTime(date);
		customer.setLastLogin(date);
		customer.setAuthProvider(provider);
		
		repo.save(customer);
	}
	
	public void loginNewCusomer(String email, String fullName, AuthProvider provider) {
		Customer customer = new Customer();
		Date date = new Date();
		
		customer.setEmail(email);
		customer.setFirstName(fullName);
		customer.setEnabled(true);
		customer.setCreatedTime(date);
		customer.setLastLogin(date);
		customer.setAuthProvider(provider);
		
		repo.save(customer);
	}

	public void updateCustomer1(Customer customer, String fullName, AuthProvider provider) {
		
		customer.setFirstName(fullName);
		customer.setAuthProvider(provider);
		customer.setLastLogin(new Date());
		
		repo.save(customer);
			
	}

	public void loginNewCustomer(Customer customer, AuthProvider provider) {
		Date date = new Date();
		
		customer.setEnabled(true);
		customer.setCreatedTime(date);
		customer.setLastLogin(date);
		customer.setAuthProvider(provider);
		
		repo.save(customer);
	}
}

