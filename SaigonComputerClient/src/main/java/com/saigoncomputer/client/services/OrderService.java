package com.saigoncomputer.client.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saigoncomputer.client.dao.OrderRepository;
import com.saigoncomputer.client.models.CartInfo;
import com.saigoncomputer.client.models.CartLineInfo;
import com.saigoncomputer.client.models.Order;
import com.saigoncomputer.client.models.OrderDetail;
import com.saigoncomputer.client.models.ShipInfo;


@Service
public class OrderService {
	
	@Autowired
	private OrderRepository repo;
	
	//For admin app
	public List<Order> listNewOrders() {
		return repo.getNewOrders();
	}
	
	public void saveOrder(CartInfo cartInfo) {
		Date date = new Date();
		
		long orderNumber = date.getTime();
		
		cartInfo.setOrderNumber(orderNumber);
		
		ShipInfo shipInfo = cartInfo.getShipInfo();
		
		Order newOrder = new Order();
		
		newOrder.setOrderNum(orderNumber);
		newOrder.setOrderDate(date);
		newOrder.setAmount(cartInfo.getAmountTotal());
		newOrder.setShipName(cartInfo.getShipInfo().getName());
		newOrder.setShipEmail(cartInfo.getShipInfo().getEmail());
		newOrder.setShipAddress(cartInfo.getShipInfo().getAddress());
		newOrder.setShipPhone(cartInfo.getShipInfo().getPhone());
		newOrder.setStatus("NEW");
		
		for (CartLineInfo line : cartInfo.getCartLines() ) {
			OrderDetail orderDetail = new OrderDetail();
			
			orderDetail.setProduct(line.getProduct());
			orderDetail.setOrder(newOrder);
			orderDetail.setPrice(line.getProduct().getPrice());
			orderDetail.setQuantity(line.getQuantity());
			
			newOrder.getOrderDetails().add(orderDetail);
		}
		repo.save(newOrder);
	}
}
