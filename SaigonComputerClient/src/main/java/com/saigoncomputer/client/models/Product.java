package com.saigoncomputer.client.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.saigoncomputer.client.helpers.Constant;
@Entity
public class Product {
	private Long id;
	private String name;
	private String type;
	private Float price;
	
	@Column(name = "image_path")
	private String imagePath;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	@Transient
	public String getFullPath() {
		if (imagePath == null || id == null) {
			return null;
		}
		
		String path = "/" + Constant.UPLOAD_FOLDER + "/" + id + "/" + imagePath;
		System.out.println("Product image full path: " + path);
		
		return path;
	}
}
