package com.saigoncomputer.client.models;

import java.util.ArrayList;
import java.util.List;

public class CartInfo {
	private long orderNUmber;
	
	private ShipInfo shipInfo;
	
	private List<CartLineInfo> cartLines = new ArrayList<>();
	
	public long getOrderNumber() {
		return orderNUmber;
	}
	
	public void setOrderNumber(long orderNumber) {
		this.orderNUmber=orderNumber;
	}

	public long getOrderNUmber() {
		return orderNUmber;
	}

	public void setOrderNUmber(long orderNUmber) {
		this.orderNUmber = orderNUmber;
	}

	public ShipInfo getShipInfo() {
		return shipInfo;
	}

	public void setShipInfo(ShipInfo shipInfo) {
		this.shipInfo = shipInfo;
	}

	public List<CartLineInfo> getCartLines() {
		return cartLines;
	}

	public void setCartLines(List<CartLineInfo> cartLines) {
		this.cartLines = cartLines;
	}
	
	public void addProduct(Product product,int quantity) {
		if(quantity<=0) {
			return;
		}
		CartLineInfo cartLineInfo = findCardLineProduct(product.getId());
		if(cartLineInfo==null) {
			cartLineInfo = new CartLineInfo();
			cartLineInfo.setProduct(product);
			cartLineInfo.setQuantity(quantity);
			cartLines.add(cartLineInfo);
		}else {
			int newQuantity = cartLineInfo.getQuantity() + quantity;
			cartLineInfo.setQuantity(newQuantity);
		}
	}
	
	public void updateProduct(long id,int quantity) {
		CartLineInfo cartLineInfo = findCardLineProduct(id);
		if(cartLineInfo != null) {
			cartLineInfo.setQuantity(quantity);
		}
	}
	
	public void removeProduct(Product product) {
		CartLineInfo cartLineInfo = findCardLineProduct(product.getId());
		if(cartLineInfo != null) {
			cartLines.remove(cartLineInfo);
		}
	}
	
	public boolean isEmpty() {
		return cartLines.isEmpty();
	}
	
	public int getQuantityTotal() {
		int quantity = 0;
		for(CartLineInfo line : cartLines) {
			quantity += line.getQuantity();
		}
		return quantity;
	}
	
	public double getAmountTotal() {
		double totalAmount =0;
		for(CartLineInfo line : cartLines) {
			totalAmount += line.getAmount();
		}
		return totalAmount;
	}
	
	public void updateQuantity(CartInfo cartForm)
	{
		if(cartForm!=null) {
			List<CartLineInfo> lines = cartForm.getCartLines();
			for(CartLineInfo line : lines) {
				if(line.getProduct() != null) {
					this.updateProduct(line.getProduct().getId(), line.getQuantity());
				}
			}
		}
	}
	
	private CartLineInfo findCardLineProduct(long id) {
		for(CartLineInfo line : cartLines) {
			if(line.getProduct().getId() == id) {
				return line;
			}
		}
		return null;
	}
}
