package com.saigoncomputer.client.helpers;

public enum AuthProvider {
	BASIC,
	FACEBOOK,
	GOOGLE;
}
