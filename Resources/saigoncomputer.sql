drop database if exists saigoncomputer;
create database saigoncomputer;
use saigoncomputer;



CREATE table customers (
  id int(11) NOT NULL AUTO_INCREMENT ,
  email varchar(255) ,
  password varchar(255) ,
  first_name varchar(255),
  last_name varchar(255),
  phone_number varchar(255),
  address varchar(255),
  city varchar(255),
  created_time varchar(255),
  last_login varchar(255),
  enabled boolean,
  auth_provider varchar(255),
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE product (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(45) ,
  type varchar(45) ,
  price int,
  image_path varchar(255),
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
create table users(
	user_id int auto_increment not null,
    id int ,
	username varchar(30) ,
    password varchar(100) ,
    enable boolean,
    primary key(user_id)
);
create table roles(
	role_id int auto_increment not null,
    name varchar(30) ,
    primary key(role_iD)
);
create table users_roles(
user_id int not null,
role_id int not null,
primary key(user_id,role_id),
constraint fk_userid foreign key(user_id) references users(user_id),
constraint fk_roleid foreign key(role_id) references roles(role_id)
);

create table Orders(
	id int auto_increment not null,
    order_time varchar(255),
    order_num int ,
    amount long,
    ship_name varchar(255),
    ship_address varchar(255),
    ship_email varchar(255),
    ship_phone varchar(255),
    status varchar(225),
    primary key(id)
);

create table Order_Details(
id int auto_increment not null,
product_id int not null,
order_id bigint not null,
quantity int,
price int,
constraint order_detail_ord_fk foreign key(order_id) references orders(id),
constraint order_detail_prod_fk foreign key(product_id) references product(id),
primary key(id)
);

insert into  customers(email,password,first_name,last_name,phone_number,address,city,created_time,last_login,enabled,auth_provider) values("duong minh thanh1999td@gmail.com","thanh1999","duong","thanh",0357018090,"789 xo viet nghe tinh","hcm","8:00","2/1/2021","1","duong minh thanh");
insert into  customers(email,password,first_name,last_name,phone_number,address,city,created_time,last_login,enabled,auth_provider) values("docongchi@gmail.com","docongchi123","do","cong chi",0359396991,"789 dang van bi","hcm","8:30","2/1/2021","0","duong minh thanh");
insert into  customers(email,password,first_name,last_name,phone_number,address,city,created_time,last_login,enabled,auth_provider) values("duongtranhsonlong@gmail.com","sl123","duong","son long",0396088936,"145 binh quoi","hcm","9:00","2/1/2021","0","duong minh thanh");
insert into  customers(email,password,first_name,last_name,phone_number,address,city,created_time,last_login,enabled,auth_provider) values("nguyenthanhtung@gmail.com","tungnt123","nguyen","tung",0395017106,"592 vo van ngan","hcm","9:30","2/1/2021","0","duong minh thanh");
insert into  customers(email,password,first_name,last_name,phone_number,address,city,created_time,last_login,enabled,auth_provider) values("vungochao@gmail.com","hao123","vu","ngoc hao",0253593456,"862 pham van dong","hcm","10:00","2/1/2021","0","duong minh thanh");

insert into product(id,name,type,price,image_path) values(1,"asus fx504vd","laptop",21000000,"h1");
insert into product(id,name,type,price,image_path) values(2,"macbook air m1","laptop",27500000,"h2");
insert into product(id,name,type,price,image_path) values(3,"acer nitro 5","laptop",19000000,"h3");
insert into product(id,name,type,price,image_path) values(4,"msi ge66","laptop",53390000,"h4");
insert into product(id,name,type,price,image_path) values(5,"dell gaming g3","laptop",28890000,"h5");
insert into product(id,name,type,price,image_path) values(6,"dell gaming g3","laptop",28890000,"h6");

insert into users (id,username,password,enable) values(1,"admin","$2a$10$cLD3cCKTm7QIPMEJp54YH.Oytv8UkDdvy9DY45r9MlFzYyN/MHif",true);
insert into users (id,username,password,enable) values(2,"vu ngoc tuan anh","$2a$10$cLD3cCKTm7QIPMEJp54YH.Oytv8UkDdvy9DY45r9MlFzYyN/MHif",false);
insert into users (id,username,password,enable)  values(3,"luong the ving","$2a$10$cLD3cCKTm7QIPMEJp54YH.Oytv8UkDdvy9DY45r9MlFzYyN/MHif",false);
insert into users (id,username,password,enable)  values(4,"cao van son","jhsddb-dasbj-adsd",false);
insert into users (id,username,password,enable)  values(5,"do nhat ha","hdeb-adsja-adejads",false);

insert into roles(name) values("manager");
insert into roles(name) values("security");
insert into roles(name) values("staff");
insert into roles(name) values("receptionist");
insert into roles(name) values("director");
insert into roles(name) values("technician");

insert into users_roles(user_id,role_id) values(1,1);
insert into users_roles(user_id,role_id) values(2,2);
insert into users_roles(user_id,role_id) values(3,3);
insert into users_roles(user_id,role_id) values(4,4);
insert into users_roles(user_id,role_id) values(5,5);
 
 insert into orders(order_time,oder_num,amount,ship_name,_ship_address) values("8:00",20,"duong minh thanh","789 xo viet nghe tinh","duongminhthanh1999td@gmail.com","0357018090","available");
 insert into orders values("8:00",20,"duong minh thanh","789 xo viet nghe tinh","duongminhthanh1999td@gmail.com","0357018090","available");
 
 insert into order_details() values(20,3200000,20);
